! ===========================================================================
!  2D model of piezoelectric extension actuator (EV-simulation)
!  The model is taken from
!    Benjeddou, A., Trindade, M.A., Ohayon, R.,
!    "A unified beam finite element model for extension and shear piezoelectric
!    actuation mechanisms", Journal of Intelligent Material Systems and Structures,
!    1997. vol. 8(12), p.1012-1025
! ===========================================================================

fini
/clear
/filname,ExtensionActuatorEV2D,1
/prep7

! initialize mesh interface
init


! ===========================
!  GEOMETRIC DEFINITIONS
! ===========================

eps=1e-6

! length of structure
l=50e-3

! length of piezo actuators
a=20e-3

! thickness of piezo actuators
t=0.5e-3

! distance of pioezo midpoint from boundary
dc=11e-3

! thickness of beam
h=2e-3

! define element size
esz=2e-3

! ===========================
!  CREATE GEOMETRY
! ===========================
rect,0,l,-h/2,h/2
rect,dc-a/2,dc+a/2,-h/2-t,-h/2
rect,dc-a/2,dc+a/2,h/2,h/2+t


allsel
aglue,all


! ===========================
!  CREATE COMPONENTS
! ===========================
asel,s,loc,x,0,l
asel,r,loc,y,-h/2,h/2
cm,bar,area

asel,s,loc,y,h/2,h/2+t
cm,piezo-top,area

asel,s,loc,y,-h/2-t,-h/2
cm,piezo-bot,area

! mechanical boundaries
lsel,s,loc,x,0
cm,left,line

lsel,s,loc,x,l
cm,right,line

! elecrical boundaries
lsel,s,loc,y,-h/2
lsel,a,loc,y,h/2
lsel,r,loc,x,dc-a/2,dc+a/2
cm,gnd,line

lsel,s,loc,y,-t-h/2
lsel,a,loc,y,t+h/2
lsel,r,loc,x,dc-a/2,dc+a/2
cm,hot,line

! keypoint for history node
ksel,s,loc,x,l
ksel,r,loc,y,h/2
cm,hist,kp

! ===========================
!  CREATE MESH
! ===========================

setelems,'quadr'
esize,esz
mshkey,2
allsel
amesh,all

! create surface mesh
setelems,'2d-line'
cmsel,s,left
cmsel,a,right
cmsel,a,hot
cmsel,a,gnd
lmesh,all

! compress element numbers
allsel
numcmp,elem

! ===========================
!  WRITE MESH
! ===========================

allsel
wnodes

cmsel,s,bar
esla
welems,'bar'

cmsel,s,piezo-top
esla
welems,'piezo-top'

cmsel,s,piezo-bot
esla
welems,'piezo-bot'

cmsel,s,left
esll
welems,'left'

cmsel,s,right
esll
welems,'right'

cmsel,s,gnd
esll
welems,'gnd'

cmsel,s,hot
esll
welems,'hot'

cmsel,s,hist
nslk
wnodbc,'hist'

mkmesh
