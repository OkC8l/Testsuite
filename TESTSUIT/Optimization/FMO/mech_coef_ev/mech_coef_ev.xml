<?xml version="1.0" encoding="UTF-8"?>

<cfsSimulation xmlns="http://www.cfs++.org/simulation">

  <documentation>
    <title>Two Scale Optimization of the mechanic coefficient for Eigenvalue maximization</title>
    <authors>
      <author>Fabian Wein</author>
    </authors>
    <date>2015-04-23</date>
    <keywords>
      <keyword>SIMP</keyword>
    </keywords>
    <references>FMO</references>
    <isVerified>no</isVerified>
    <description>Similar to FMO but only diagonal coefficients are optimized, therefore no defnitieness constraints are necessary.
      Additionally the mass is a design variable. It needs to be scaled to have a meaningful design range.
      The problem is that the first two mode become quickly double modes and we have no working mechanism against it yet.

      Simply visualuze via matviz.py results_hdf5/mech_coef_ev.h5
    </description>
  </documentation>

  <fileFormats>
    <output>
      <hdf5 />
      <info />
    </output>
    <materialData file="mat.xml" format="xml" />
  </fileFormats>

  <domain geometryType="plane">
    <regionList>
      <region material="99lines" name="mech" />
    </regionList>
  </domain>

  <sequenceStep index="1">
    <analysis>
      <eigenFrequency>
        <isQuadratic>no</isQuadratic>
        <!-- one mode leads to problems with Arpack, possibly because it is a double mode ?! -->
        <numModes>2</numModes>
        <freqShift>0</freqShift>
        <writeModes>yes</writeModes>
      </eigenFrequency>
    </analysis>

    <pdeList>
      <mechanic subType="planeStrain">
        <regionList>
          <region name="mech" />
        </regionList>

        <bcsAndLoads>
          <fix name="north">
            <comp dof="x" />
            <comp dof="y" />
          </fix>
          <fix name="east">
            <comp dof="x" />
            <comp dof="y" />
          </fix>
          <fix name="south">
            <comp dof="x" />
            <comp dof="y" />
          </fix>
          <fix name="west">
            <comp dof="x" />
            <comp dof="y" />
          </fix>
        </bcsAndLoads>
        <storeResults>
          <nodeResult type="mechDisplacement">
            <allRegions />
          </nodeResult>
          <!-- for visualization -->
          <elemResult type="mechTensor">
            <allRegions />
          </elemResult>
          <!-- contains the design parameters -->
          <elemResult type="mechTensorHillMandel">
            <allRegions />
          </elemResult>
          <!-- the mass design variable -->
          <elemResult type="optResult_1">
            <allRegions />
          </elemResult>
        </storeResults>
      </mechanic>
    </pdeList>
  </sequenceStep>

  <optimization>
    <costFunction type="slack" task="maximize">
      <stopping queue="999" value="0.002" type="designChange" />
    </costFunction>

    <constraint type="eigenfrequency" bound="lowerBound" value="slack" ev="1" />
    <constraint type="eigenfrequency" bound="lowerBound" value="slack" ev="2" mode="constraint" />

    <constraint type="volume" bound="upperBound" value="0.5" design="mech_trace" linear="true" notation="hill_mandel" />
    <constraint type="tensorTrace" bound="upperBound" value="1.0" design="mech_trace" linear="true" notation="hill_mandel" />

    <constraint type="volume" bound="equal" value="1.0" design="mass" />

    <optimizer type="snopt" maxIterations="20">
      <snopt>
        <option name="verify_level" type="integer" value="0" />
      </snopt>
    </optimizer>

    <ersatzMaterial region="mech" material="mechanic" method="paramMat">
      <paramMat>
        <designMaterials>
          <designMaterial type="fmo" optimizeMass="true" massFactor="1e-8">
            <param name="mech_12" value="0.0" />
            <param name="mech_13" value="0.0" />
            <param name="mech_23" value="0.0" />
          </designMaterial>
        </designMaterials>
      </paramMat>
      <design name="mech_11" initial="0.33" lower="0.1" upper="1.0" />
      <design name="mech_22" initial="0.33" lower="0.1" upper="1.0" />
      <design name="mech_33" initial="0.33" lower="0.1" upper="1.0" />
      <design name="mass" initial="1" lower="0.5" upper="2" />
      <design name="slack" initial="3500" lower="0" upper="1e4" />
      <result value="design" id="optResult_1" design="mass" />
      <export />
    </ersatzMaterial>
    <commit mode="each_forward" stride="999" />
  </optimization>


</cfsSimulation>


