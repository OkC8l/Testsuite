<?xml version="1.0" encoding="UTF-8"?>

<cfsSimulation xmlns="http://www.cfs++.org/simulation">
  
  <documentation>
    <title>Heat_conduction_two_materials_2d</title>
    <authors>
      <author>Bich Ngoc Vu</author>
    </authors>
    <date>2020-03-31</date>
    <keywords>
      <keyword>SIMP</keyword>
    </keywords>
    <references>Andreassen et al.: How to determine composite material properties using numerical homogenization </references>
    <isVerified>yes</isVerified>
    <description> Demonstrate the calculation of thermal conductivity by asymptotic homogenization of the static
    heat equation.
    Very similar to linear elasticity, except that test strains consist only of unit vectors. Thus,
    the result is a rank-2 thermal conductivity tensor of size 2 x 2 (2D) or 3 x 3 (3D). 
    </description>
  </documentation>  
  
  <fileFormats>
    <output>
      <hdf5 directory="./" />
      <info/>
    </output>
    <materialData file="mat.xml" format="xml" />
  </fileFormats>

  <domain geometryType="plane">
    <regionList>
      <region name="mech" material="water"/>
      <region name="void" material="air"/>
    </regionList>
  </domain>

  <sequenceStep index="1">
    <analysis>
      <static/>
    </analysis>

    <pdeList>
      <heatConduction>
        <regionList>
          <region name="mech"/>
          <region name="void"/>
        </regionList>

        <bcsAndLoads>
           <periodic slave="west" master="east" quantity="heatTemperature"/>
           <periodic slave="south"  master="north" quantity="heatTemperature" />
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="heatTemperature">
            <allRegions/>
          </nodeResult>
          <elemResult type="mechPseudoDensity">
            <allRegions/>
          </elemResult>
       </storeResults>
      </heatConduction>
    </pdeList>
    <linearSystems>
      <system>
        <solverList>
          <directLDL/> 
        </solverList>
      </system>
    </linearSystems>
  </sequenceStep>

  <optimization log="">
    <costFunction type="homTensor" task="minimize" multiple_excitation="true">
      <multipleExcitation type="homogenizationTestStrains" sequence="1"/>
    </costFunction>
    <optimizer type="evaluate" maxIterations="1"/>
    <ersatzMaterial method="simp" material="heat">
      <regions>
        <region name="mech"/>
        <region name="void"/>
      </regions>
      <design name="density" initial="1" lower="1e-3" upper="1" region="mech"/>
      <design name="density" initial="1" lower="1e-3" upper="1" region="void"/>
      <transferFunction type="identity" application="heat" design="density"/>
    </ersatzMaterial>
    <commit mode="each_forward"/>
  </optimization>  
</cfsSimulation>


