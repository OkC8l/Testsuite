! ===========================================================================
!  3D model of a mechanic plate, loaded by uniform pressure.
! ===========================================================================

fini
/clear
/filname,Plated3D,1
/prep7

! initialize mesh interface
init


! ===========================
!  GEOMETRIC DEFINITIONS
! ===========================

eps=1e-6
mSize=0.5
t=.35

! ===========================
!  CREATE GEOMETRY
! ===========================
rect,0,12,0,12

rect,0,12,0,0.5
rect,0,12,4.8,7.2
rect,0,12,7.2,11.5

rect,0,0.5,0,12
rect,4.8,7.2,0,12
rect,7.2,11.5,0,12

aovlap,all

! ===========================
!  CREATE 2D MESH 
! ===========================

setelems,'quadr'
esize,,1
mshkey,2
mshape,0,2D
amesh,all


! ===========================
!  EXTRUDE TO 3D MESH
! ===========================
esize,,1
setelems,'brick'
vext,all,,,,,t


! clear all surface element prior to generating
! new ones
allsel
asel,all
aclear,all

! generate surface mesh at corner of plates to fix
setelems,'quadr'
asel,s,loc,x,0
asel,a,loc,x,12
asel,a,loc,y,0
asel,a,loc,y,12
cm,side,area
amesh,all

! generate surface mesh at top of structure
asel,s,loc,z,t
cm,top,area
amesh,all

! compress element numbers
allsel
numcmp,elem

! ===========================
!  WRITE MESH
! ===========================

allsel
wnodes

allsel
eslv
welems,'plate'

cmsel,s,side
esla
welems,'side'

cmsel,s,top
esla
welems,'top'

mkmesh
